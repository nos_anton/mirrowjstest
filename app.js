var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var emitter = require('events').EventEmitter;
var app = {
    url: 'mongodb://localhost:27017/morrowjstest'
};

//====================================================

app.on('insert',function(collectionName,item){
    app.createDBConnection(function(db,cb){
        var collection = db.collection(collectionName);
        collection.insert(item);
        cb();
    });
});

app.on('removeById',function(collectionName,_id){
    app.createDBConnection(function(db,cb){
        var collection = db.collection(collectionName);
        collection.insert({_id:_id});
        cb();
    });
});

app.on('update',function(collectionName,item){
    app.createDBConnection(function(db,cb){
        var collection = db.collection(collectionName);
        collection.update(item);
        cb();
    });
});

app.on('find',function(collectionName,findProps,findParams,findCB){
    app.createDBConnection(function(db,cb){
        var collection = db.collection(collectionName);
        var res = collection.find(findProps,findParams);
        findCB(res);
        cb();
    });
})

//==================================================

app.createDBConnection = function(func){
    MongoClient.connect(app.url, function(){
        assert.equal(null, err);
        console.log("Connected correctly to server");

        func(db,function(){
            db.close();
        })
    });
}